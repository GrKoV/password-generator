(function() {
  'use strict';

  // Find input tags;
  var tags = document.getElementsByTagName('input');
  var tagsArr = Array.from(tags);

  // Hide save label.
  var saved = document.getElementsByClassName('js-saved')[0];
  saved.style.display = 'none';

  // Set values from storage.
  chrome.storage.sync.get(null, function(options) {
    tagsArr.forEach(function(tag) {
      tag.onchange = tag.onchange = handlerSaveOptions;
      var value = options[tag.name];

      if (tag.type === 'text' && value) {
        tag.value = value;
      }
      else if (tag.type === 'checkbox') {
        tag.checked = value;
      }
    });
  });

  /**
   * Options save handler.
   */
  function handlerSaveOptions() {
    var options = tagsArr.reduce(function(options, tag) {
      if (tag.type === 'text') {
        options[tag.name] = tag.value;
      }
      else if (tag.type === 'checkbox') {
        options[tag.name] = tag.checked;
      }

      return options;
    }, {});

    chrome.storage.sync.set(options, function() {
      saved.style.display = 'block';
      setTimeout(function() {
        saved.style.display = 'none';
      }, 750);
    });
  }
})();