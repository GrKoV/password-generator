(function() {
  'use strict';

  // Catch input element where clicked right button mouse.
  var inputEl;
  document.addEventListener('mousedown', function(evt) {
    //  Right btn == 2
    if (evt.button !== 2 || evt.target.nodeName !== 'INPUT') return;

    inputEl = evt.target;
  });

  // Subscribe on background commands.
  chrome.runtime.onMessage.addListener(function(options) {
    options = options || {};

    if (!inputEl || options.action !== 'generate_password') return;

    var password = generatePassword(options);

    // Fill in other fields of type password.
    var passwordTags = findInputPasswordTags();
    passwordTags.push(inputEl);
    passwordTags.map(function(tag) {
      tag.value = password;
    });

    // Copy password to clipboard.
    copyToClipboard(inputEl);
  });

  /**
   * Generate password.
   *
   * @param {Object} options
   * - {Number} passwordLength Password length;
   * - {Boolean} russian Include russian symbols;
   * - {Boolean} numbers Include numbers;
   * - {Boolean} symbols Include symbols;
   * @returns {String} String password.
   */
  function generatePassword(options) {
    options = options || {};
    var length = options.passwordLength || 12;
    var dataSet = {
      english: 'abcdefghijklnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ',
      numbers: '0123456789',
      symbols: '!@#$%^&*()}{"№;%:?',
      russian: 'абвгдеёжзийклмнопрстуфхцчшщъыьэюяАБВГДЕЁЖЗИЙКЛМНОПРСТУФХЦЧШЩЪЫЬЭЮЯ'
    };

    // Default only english.
    var dataSetStr = dataSet.english;
    var retVal = '';

    // Convert dataSet to string.
    Object.keys(dataSet).map(function(key) {
      if (options[key]) {
        dataSetStr += dataSet[key];
      }
    });

    // Random select symbol.
    while (length) {
      length--;
      var index = Math.floor(Math.random() * dataSetStr.length);
      retVal += dataSetStr[index];
    }

    return retVal;
  }

  /**
   * Finds input password tags in document.
   *
   * @returns {Object[]} Return array password nodes.
   */
  function findInputPasswordTags() {
    var tags = document.getElementsByTagName('input');

    return Array.from(tags).reduce(function(passwordTags, tag) {
      if (tag.type.toLowerCase() === 'password') {
        passwordTags.push(tag);
      }

      return passwordTags;
    }, []);
  }

  /**
   * Copy to clipboard text from inputEl.
   *
   * @param {Object} inputEl DOM element,
   */
  function copyToClipboard(inputEl) {
    inputEl.focus();
    inputEl.setSelectionRange(0, inputEl.value.length);
    inputEl.addEventListener('copy', function(evt) {
      evt.preventDefault();
      evt.clipboardData.setData('text/plain', inputEl.value);
    });

    try {
      document.execCommand('copy', false);
    }
    catch (err) {
      console.error('Error copy to clipboard password:', inputEl.value);
    }
    inputEl.removeEventListener('copy');
    window.getSelection().removeAllRanges();
  }
})();