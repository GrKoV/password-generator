(function() {
  'use strict';

  chrome.contextMenus.create({
    title: 'Generate password',
    contexts: ['editable'],
    onclick: onClickHandler
  });

  function onClickHandler(item, tab) {
    // Run generate password.
    chrome.storage.sync.get(null, function(options) {
      chrome.tabs.sendMessage(tab.id, Object.assign({
        action: 'generate_password'
      }, options));
    });
  }
})();